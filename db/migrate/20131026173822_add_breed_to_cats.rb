class AddBreedToCats < ActiveRecord::Migration
  def change
    add_column :cats, :breed, :string, default: 'Unknown'
  end
end
