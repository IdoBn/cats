class Cat < ActiveRecord::Base
	mount_uploader :image, ImageUploader
  attr_accessible :bio, :name, :breed, :image

  BREEDS = ['Abyssinian', 'Aegean Cat', 'Begal', 'Siamese', 'Ragdoll', 'Persian']

  validates_presence_of :name, :user_id
  before_save :short_bio
  before_update :short_bio
  after_create :notify_followers

  belongs_to :user
  has_many :likes,  :dependent => :destroy
  has_many :likers, :through => :likes, :source => :user

  scope :not_owned_by, lambda { |user|
    user ? where('user_id <> ?', user.id) : scoped
  }

  default_scope { order('cats.created_at desc') }

  BREEDS.each do |breed|
    scope breed.gsub(' ', '_').downcase.to_sym, lambda { where(breed: breed) }
  end

  scope :most_liked, lambda { order('likes') }
  scope :by_breed, lambda { |breed| where('breed = ?', breed)} #the BREED scope does this for you

  def self.not_owned_by(user)
  	#where('user_id <> ?', user.id)
  	user ? where('user_id <> ?', user.id) :scoped
  end
  def short_bio
    self.bio = self.bio[0..50]
  end

  private

  def notify_followers
    self.user.followers.each do |follower|
      CatMailer.notify_follower_new_cat(follower, self).deliver
    end
  end
end 
