class User < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :age, :first_name, :last_name, :city, :user_name, :image

  validates_presence_of :email, :password, :age, :first_name, :last_name, :city, :user_name
  validates_uniqueness_of :email, :user_name

  before_validation :ensure_presence_of

  has_many :cats, dependent: :destroy # :dependant => :destroy
  has_many :likes,  :dependent => :destroy
  has_many :liked_cats, :through => :likes, :source => :cat


  has_many :following_users, class_name: 'Follow', foreign_key: 'followed_user_id'
  has_many :followers, through: :following_users, source: :follower

  has_many :followings, class_name: 'Follow', foreign_key: 'user_id'
  has_many :users_followed, through: :followings, source: :followed_user


  def full_name
      "#{first_name} #{last_name}"
  end

  def owns?(cat)
      cat.user == self
  end

  def to_s
    full_name 
  end

  def likes?(cat)
    liked_cats.include? cat
  end

  def like!(cat)
    liked_cats << cat unless likes?(cat)
    #likes?(cat) ? liked_cats(cat).destroy : liked_cats << cat
  end

  def unlike!(cat)
    likes.find_by_cat_id(cat.id).try(:destroy)
  end

  def follows?(user)
    users_followed.include? user
  end

  def follow!(user)
    users_followed << user unless follows?(user)   
  end

  def unfollow!(user)
    users_followed.destroy user
  end  

  protected

    def ensure_presence_of
      self.first_name = 'NoFirstName' if first_name.blank?
      self.last_name = 'NoLastName' if last_name.blank?
    end



end
