class Follow < ActiveRecord::Base
  belongs_to :follower, class_name: 'User', foreign_key: 'user_id'
  belongs_to :followed_user, class_name: 'User', foreign_key: 'followed_user_id'

  validates_uniqueness_of :followed_user_id, scope:[:user_id]
  validate :follow_yourself
  #validate :unique_follow


  def follow_yourself
  	errors.add 'you can not follow yourself' if user_id == followed_user_id
  end
  # def unique_follow
  # 	errors.add :followed_user_id, 'this connection exits already' if Follow.where(followed_user_id: followed_user_id, user_id: user_id).any?
  # end
end
