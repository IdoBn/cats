module UsersHelper

	def follow_button(user)
		if !current_user.follows?(user) and current_user != user
      link_to "Follow", follow_user_path(user), method: :post, class: 'button radius'
    elsif current_user.follows?(user)
      link_to "Unfollow", unfollow_user_path(user), method: :delete, class: 'button radius alert'
    end
  end
	
end
