module CatsHelper

	def like_button(cat)
		unless current_user && current_user.likes?(cat)
			link_to "Like", like_cat_path(cat), class: 'button radius small', method: :post 
 		else
			link_to "Unlike", unlike_cat_path(cat), class: 'button radius small alert', method: :delete 
 		end
	end

	def like_button_count(cat)
		if current_user && current_user.likes?(cat) && cat.likes.count > 2
      	"<h6>You and #{cat.likes.count - 1} other people like this cat</h6>".html_safe
    elsif current_user && current_user.likes?(cat) && cat.likes.count > 1
        "<h6>You and one other person likes this cat</h6>".html_safe
    elsif current_user && current_user.likes?(cat)
        "<h6>You like this cat</h6>".html_safe
    elsif cat.likes.count > 0 
    		"<h6>#{cat.likes.count} #{ cat.likes.count > 1 ? 'people like this cat' : 'person likes this cat' }</h6>".html_safe
    end
	end
end