class CatMailer < ActionMailer::Base
  default from: "Ido@cats.com"

  def new_like(cat, liker)
    @liker = liker
    @user  = cat.user
    @cat   = cat
    mail(to: @user.email, subject: "#{liker.full_name} just liked your cat!")
  end

  def new_unlike(cat, unliker)
  	@unliker = unliker
  	@user = cat.user
  	@cat = cat
  	mail(to: @user.email, subject: "#{unliker.full_name} just UNLIKED your cat!")
  end

  def notify_follower_new_cat(follower, cat)
    @follower = follower
    @cat      = cat
    mail(to: @follower.email, subject: "#{@cat.user.try(:full_name)} just uploaded a new cat!")      
  end

end
