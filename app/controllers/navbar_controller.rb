class NavbarController < ApplicationController

	def index
		@all_cats = Cat.all
		if user_signed_in?
			@my_cats = current_user.cats
		end
	end

end