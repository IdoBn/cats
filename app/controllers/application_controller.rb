class ApplicationController < ActionController::Base
  protect_from_forgery

  def deny
  	redirect_to root_path, alert: "You don't have permission to see this page."
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
end
