class PagesController < ApplicationController

	def index
    user_ids = current_user.users_followed.map(&:id) if current_user 
    @cats = current_user ? Cat.where(user_id: user_ids).paginate(:page => params[:page], :per_page => 6) : Cat.paginate(:page => params[:page], :per_page => 6)
    #@cats = Cat.all if @cats.empty?
  end

end