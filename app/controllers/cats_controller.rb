class CatsController < ApplicationController

	before_filter :authenticate_user!
	before_filter :load_cat, :only => [:destroy, :update, :edit]
	before_filter :load_user, :only => [:index]
	before_filter :authorize_cat!, :only => [:edit, :update, :destroy]

	def new
		@cat = Cat.new
	end

	def create
		@cat = current_user.cats.new(params[:cat])
		if @cat.save
			flash[:notice] = "Created cat successfuly!"
			redirect_to cats_path
		else
			render 'new'
		end
	end

	def index
		if params[:user_id]
    	@user = User.find_by_id(params[:user_id])
  	else
    	@user = current_user
  	end
  	@cats = @user.cats
	end

	def destroy
		#@cat = current_user.cats.find(params[:id])
		if @cat.destroy
			flash[:notice] = "Cat destroyed!"
		else
			flash[:alert] = "Cat is Still alive!"
		end
		redirect_to cats_path #also :back
	end

	def edit
		#@cat = current_user.cat
	end

	def update
		#@cat = current_user.cats.find(params[:id])
		if @cat.update_attributes(params[:cat])
			redirect_to cats_path, :notice => "Cat updated!"
		else
			render 'edit'
		end
	end

	def like
		@cat = Cat.find(params[:id])
		current_user.like!(@cat)
		CatMailer.new_like(@cat, current_user).deliver
		redirect_to :back, notice: "You liked #{current_user.full_name}'s cat: #{@cat.name}"
	end

	 def unlike
    @cat = Cat.find(params[:id])
    current_user.unlike!(@cat)
    CatMailer.new_unlike(@cat, current_user).deliver
    redirect_to :back, notice: "You unliked #{@cat.name}"
  end


	private

		def load_cat
			@cat = Cat.find(params[:id])
		end

		def load_user
			if params[:user_id]
				@user = User.find(params[:user_id])
			else
				@user = current_user
			end
		end

		def authorize_cat!
			deny unless current_user.owns?(@cat)
		end

end




