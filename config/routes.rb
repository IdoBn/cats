Cats::Application.routes.draw do

  devise_for :users

  resources :cats do
  	member do
  		post :like
      delete :unlike
  	end
  end

  resources :users, only: [:index, :show] do
  	member do
      get :cat
      post :follow
      delete :unfollow
    end
    resources :cats, :only => [:index]
    #resources :like, :on => :member #collection
	end 

  match 'favcats', :to => 'users#favcats'
root :to => 'pages#index'

end
